jitterentropy-rngd (1.2.8-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 13:52:08 +0000

jitterentropy-rngd (1.2.8-3) unstable; urgency=medium

  * d/control: bump Standards-Version to 4.7.0, no changes
  * Switch to pkgconf
  * Update d/watch to use Github

 -- Luca Boccassi <bluca@debian.org>  Fri, 24 May 2024 15:09:22 +0100

jitterentropy-rngd (1.2.8-2) unstable; urgency=medium

  * Build-depend on systemd-dev instead of systemd (Closes: #1060525)
  * Bump Standards-Version to 4.6.2, no changes
  * Update Lintian overrides

 -- Luca Boccassi <bluca@debian.org>  Thu, 11 Jan 2024 23:46:42 +0000

jitterentropy-rngd (1.2.8-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 25 Sep 2024 12:42:08 +0000

jitterentropy-rngd (1.2.8-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.8'
  * Bump Standards-Version to 4.6.1, no changes
  * Update Lintian override syntax

 -- Luca Boccassi <bluca@debian.org>  Tue, 06 Sep 2022 00:39:25 +0100

jitterentropy-rngd (1.2.7-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.7'
  * Bump d/copyright year range
  * Update lintian-overrides syntax

 -- Luca Boccassi <bluca@debian.org>  Sun, 20 Mar 2022 12:50:06 +0000

jitterentropy-rngd (1.2.6-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.6'

 -- Luca Boccassi <bluca@debian.org>  Sat, 30 Oct 2021 21:55:25 +0100

jitterentropy-rngd (1.2.5-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.5'

 -- Luca Boccassi <bluca@debian.org>  Sun, 19 Sep 2021 15:13:31 +0100

jitterentropy-rngd (1.2.4-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.4'
  * Bump Standards-Version to 4.6.0, no changes

 -- Luca Boccassi <bluca@debian.org>  Thu, 02 Sep 2021 11:49:24 +0100

jitterentropy-rngd (1.2.3-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.3'
  * Drop all patches, merged upstream.
  * Build with -O0.

 -- Luca Boccassi <bluca@debian.org>  Sun, 15 Aug 2021 13:22:35 +0100

jitterentropy-rngd (1.2.1-2+apertis0) apertis; urgency=medium

  * Sync from debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 21 Jul 2021 14:02:47 +0000

jitterentropy-rngd (1.2.1-2) unstable; urgency=medium

  * Backport patches to fix 100% CPU usage bug when running on kernel
    5.11 or newer. (Closes: #985302)
  * Add d/upstream/metadata file

 -- Luca Boccassi <bluca@debian.org>  Mon, 15 Mar 2021 17:07:43 +0000

jitterentropy-rngd (1.2.1-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 08:46:12 +0000

jitterentropy-rngd (1.2.1-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.2.1'

 -- Luca Boccassi <bluca@debian.org>  Fri, 22 Jan 2021 11:43:55 +0000

jitterentropy-rngd (1.2.0-1) unstable; urgency=medium

  * Merge branch 'lintian-fixes' into 'master'
  * Update upstream source from tag 'upstream/1.2.0'
  * Bump Standards-Version to 4.5.1, no changes
  * Switch to debhelper-compat 13
  * Use https in d/copyright
  * Add Pre-Depends: ${misc:Pre-Depends}
  * Override Lintian false positive

 -- Luca Boccassi <bluca@debian.org>  Fri, 04 Dec 2020 19:55:20 +0000

jitterentropy-rngd (1.1.0-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.1.0'
  * Drop patches merged upstream in v1.1.0
  * Bump Standards-Version to 4.4.0, no changes
  * d/rules: systemd unit is installed by the makefile now
  * d/rules: CHANGES was renamed to CHANGES.md
  * Add build-dependency on pkg-config and systemd for systemd.pc
  * Enable all hardening flags, 'bind now' was missing
  * d/watch: switch to https

 -- Luca Boccassi <bluca@debian.org>  Sat, 28 Sep 2019 14:06:14 +0100

jitterentropy-rngd (1.0.8-4co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 17 Feb 2021 00:04:04 +0000

jitterentropy-rngd (1.0.8-4) unstable; urgency=medium

  [ Helmut Grohne ]
  * Don't strip during make install. (Closes: #930203)

  [ Luca Boccassi ]
  * Backport 0002-Makefile-support-cross-compiling.patch to fix
    crosscompiling. (Closes: #930203)
  * Bump Standards-Version to 4.3.0, no changes.
  * Set Rules-Requires-Root: no.

 -- Luca Boccassi <bluca@debian.org>  Sat, 08 Jun 2019 12:02:30 +0100

jitterentropy-rngd (1.0.8-3) unstable; urgency=medium

  * Add jitterentropy-rngd.init sysv script.
  * Override Lintian warning about unusual systemd target.
    The daemon needs to start as early as possible to provide entrophy
    during boot.
  * Install upstream changelog.
  * Bump Standards-Version to 4.2.1.

 -- Luca Boccassi <bluca@debian.org>  Fri, 19 Oct 2018 11:47:22 +0100

jitterentropy-rngd (1.0.8-2) unstable; urgency=medium

  * Set homepage to specific project link. (Closes: #904721)
  * Set architecture to linux-any, as this package is linux specific.
  * Bump Standards-Version to 4.1.5, no changes.

 -- Luca Boccassi <bluca@debian.org>  Fri, 27 Jul 2018 09:40:19 +0100

jitterentropy-rngd (1.0.8-1) unstable; urgency=medium

  * Initial packaging. (Closes: #901812)
  * Backport patch to fix systemd unit name and install it.

 -- Luca Boccassi <bluca@debian.org>  Thu, 21 Jun 2018 17:43:43 +0100
